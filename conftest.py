import pytest


pytest_plugins = ["aws_test_functions.plugin.aws_scp_tester"]


@pytest.fixture(scope="session")
def user_prefix():
    return "test_hipaa_scp"
