MARK ?= not slowtest and not premium_account_required
KEYWORD ?= test
ARGS ?=

test: test-without-controls test-with-controls

test-without-controls: reset-scp _test-without-controls
test-with-controls: scp _test-with-controls

_test-without-controls: setup-org
	$(MAKE) _test

_test-with-controls: test-org
	$(MAKE) _test

_test: setup-profile test-org
	pytest -m "$(MARK)" -k "$(KEYWORD)" $(ARGS)

timed-test:
	$(MAKE) test 2>&1 | ts -s '%H:%M:%.S' | ts -i '%H:%M:%.S'

slowtest: setup-profile
	$(MAKE) MARK='slowtest' test

# ----------------------------------------------------------------------------
# The following variables and targets are present to try to clean up and
# recreate the pipeline script in Bitbucket.
# ----------------------------------------------------------------------------

# These variables may be overridden by environment variables with the same name
CLOUDFORMATION_STACK_NAME ?= rhedcloud-aws-rs-account
CLOUDTRAIL_BUCKET_NAME ?= rhedcloud-pipeline-4-ct1
CLOUDTRAIL_NAME ?= $(CLOUDFORMATION_STACK_NAME)-Master
RHEDCLOUD_ACCOUNT_NAME ?= RHEDcloud Pipeline 4
RHEDCLOUD_SETUP_ORG ?= RHEDcloudAccountAdministrationOrg
RHEDCLOUD_TEST_ORG ?= RHEDcloudHipaaAccountOrg
RHEDCLOUD_SETUP_PROFILE ?= rhedcloud-pipeline-master
RHEDCLOUD_TEST_PROFILE ?= rhedcloud-pipeline-4
SCP_NAME ?= rhedcloud-aws-org-hipaa-scp
REPO_NAME ?= rhedcloud-aws-org-hipaa-scp

CHECK_CMD ?= stack-state.py check $(SCP_NAME) \
	./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
	./rhedcloud-aws-vpc-type1-cfn/rhedcloud-aws-vpc-type1-cfn.json > /dev/null 2>&1

# Download the artifacts required for the tests in this repository
get-artifacts:
	download_artifact.sh rhedcloud-aws-rs-account-cfn rhedcloud-aws-rs-account-cfn.latest.zip
	download_artifact.sh rhedcloud-aws-vpc-type1-cfn rhedcloud-aws-vpc-type1-cfn.latest.zip
	download_artifact.sh rhedcloud-aws-org-standard-scp rhedcloud-aws-org-standard-scp.latest.zip
	ln -sf rhedcloud-aws-org-standard-scp/bin ./bin
	ln -sf rhedcloud-aws-org-standard-scp/tests ./tests
	ls -l

# Ensure that we have AWS profiles setup for testing
aws-profiles:
	@aws_profile_from_env.py \
		$(RHEDCLOUD_SETUP_PROFILE) AWS_MASTER_ACCESS_KEY_ID AWS_MASTER_SECRET_ACCESS_KEY
	@aws_profile_from_env.py \
		$(RHEDCLOUD_TEST_PROFILE) AWS_TARGET_ACCESS_KEY_ID AWS_TARGET_SECRET_ACCESS_KEY

# Switch to the setup profile
setup-profile: aws-profiles
	$(eval AWS_PROFILE := $(RHEDCLOUD_SETUP_PROFILE))

# Switch to the test profile
test-profile: aws-profiles
	$(eval AWS_PROFILE := $(RHEDCLOUD_TEST_PROFILE))

# Move the AWS account to the SETUP_ORG
setup-org: setup-profile
	org_move_account.py "$(RHEDCLOUD_ACCOUNT_NAME)" $(RHEDCLOUD_SETUP_ORG)
	sleep 5

# Move the AWS account to the TEST_ORG
test-org: setup-profile
	org_move_account.py "$(RHEDCLOUD_ACCOUNT_NAME)" $(RHEDCLOUD_TEST_ORG)
	sleep 5

# Detach, recreate, and attach the SCP
scp: setup-profile
	scp_tool.py apply $(SCP_NAME).json $(RHEDCLOUD_TEST_ORG)

# Remove all but the FullAWSAccess SCP
reset-scp: setup-profile
	scp_tool.py reset $(RHEDCLOUD_TEST_ORG)

# update the trust relationship for the RHEDcloudMaintenanceOperatorRole to
# allow the user running the tests to assume that role
test-trust-relationship: setup-profile
	$(eval ident = $(shell aws sts get-caller-identity --query 'Arn' --output text))
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudAdministratorRole $(ident)
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudAuditorRole $(ident)
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudCentralAdministratorRole $(ident)
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudMaintenanceOperatorRole $(ident)
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudSecurityIRRole $(ident)
	AWS_PROFILE=$(RHEDCLOUD_TEST_PROFILE) trust_me.py trust RHEDcloudSecurityRiskDetectionServiceRole $(ident)


# Cache the checksums of each CloudFormation template used for this repository
# so we can determine whether to rebuild the stacks at a later time.
cache-states: get-artifacts
	stack-state.py save $(SCP_NAME) \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json \
		./rhedcloud-aws-vpc-type1-cfn/rhedcloud-aws-vpc-type1-cfn.json

# Retrieve necessary artifacts and run a sequence of targets to clean and
# rebuild our stacks.
rebuild: get-artifacts clean rs-account vpc-type1 script-runners

# Run a sequence of targets to clean and rebuild our stacks.
local-rebuild: clean rs-account vpc-type1 script-runners test-trust-relationship

# Utility to determine whether a stack rebuild is necessary
should-rebuild:
	bash -c "$(CHECK_CMD) && echo No rebuild necessary || echo Rebuild necessary"

# Clean up S3 buckets, runners, and Type 1 VPC if stack changes are detected
clean:
	bash -c "$(CHECK_CMD) && $(MAKE) clean-resources || $(MAKE) reset-scp clean-runners clean-resources clean-bucket clean-stack"

clean-final: reset-scp clean-runners clean-bucket clean-stack clean-stack-rsaccount

# Remove resources left behind by previous tests
clean-resources: reset-scp test-profile
	python ./bin/cleanup.py

# Remove the CloudTrail bucket
clean-bucket: test-profile
	s3_delete_bucket.py $(CLOUDTRAIL_BUCKET_NAME)

# Remove the test runner EC2 instances
clean-runners: test-profile
	rhedcloud_script_runners.py destroy || echo 'Continuing...'

# Remove the Type 1 VPC CloudFormation stack
clean-stack: reset-scp test-profile
	cfn_stack_delete.py rhedcloud-aws-vpc-type1

# Remove the RS Account CloudFormation stack
clean-stack-rsaccount: reset-scp test-profile
	cfn_stack_delete.py rhedcloud-aws-rs-account

# Create the rs-account CloudFormation stack
rs-account: test-profile
	bash -c "$(CHECK_CMD) || cfn_stack_create.py \
		--parameter CloudTrailName=${CLOUDTRAIL_BUCKET_NAME} \
		--compact $(CLOUDFORMATION_STACK_NAME) \
		./rhedcloud-aws-rs-account-cfn/rhedcloud-aws-rs-account-cfn.json"

# Create the Type 1 VPC CloudFormation stack
vpc-type1: test-profile
	bash -c "$(CHECK_CMD) || cfn_stack_create.py \
		--compact rhedcloud-aws-vpc-type1 \
		./rhedcloud-aws-vpc-type1-cfn/rhedcloud-aws-vpc-type1-cfn.json"

# Create two new EC2 instances to run scripts
script-runners: test-profile
	rhedcloud_script_runners.py setup

# Upload test artifacts
upload:
	git archive --format zip --output $(SCP_NAME).latest.zip master
	zip -rv $(SCP_NAME).latest.zip $(SCP_NAME)-with-metadata.json
	cp $(SCP_NAME)-with-metadata.json $(SCP_NAME).latest.json
	cp $(REPO_NAME).latest.zip $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip
	upload_artifact.sh $(SCP_NAME).latest.json $(SCP_NAME).latest.zip
	upload_artifact_s3.sh $(REPO_NAME)-${BITBUCKET_BRANCH}-${BITBUCKET_BUILD_NUMBER}.zip rhedcloud-bitbucket-downloads /$(REPO_NAME)/

# Promote SCP
promote: setup-profile
	upload_scp.sh $(SCP_NAME)-with-metadata.json $(BUCKET) $(OWNER_ID)


.EXPORT_ALL_VARIABLES:
.PHONY: setup-profile test-profile setup-org test-org
